package com.saiprashant.spoartsintractive;

public class MatchDetailsModel {

    String Officials="";
    String Weather="";
    String str_Winningteam="";
    String Player_Match="";
    String Result="";
    String Winmargin="";
    String Equation="";
    public String getStr_Winningteam() {
        return str_Winningteam;
    }

    public void setStr_Winningteam(String str_Winningteam) {
        this.str_Winningteam = str_Winningteam;
    }


    public String getOfficials() {
        return Officials;
    }

    public void setOfficials(String officials) {
        Officials = officials;
    }

    public String getWeather() {
        return Weather;
    }

    public void setWeather(String weather) {
        Weather = weather;
    }



    public String getPlayer_Match() {
        return Player_Match;
    }

    public void setPlayer_Match(String player_Match) {
        Player_Match = player_Match;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getWinmargin() {
        return Winmargin;
    }

    public void setWinmargin(String winmargin) {
        Winmargin = winmargin;
    }

    public String getEquation() {
        return Equation;
    }

    public void setEquation(String equation) {
        Equation = equation;
    }


}
