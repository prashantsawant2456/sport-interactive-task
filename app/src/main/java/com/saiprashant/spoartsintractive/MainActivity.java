package com.saiprashant.spoartsintractive;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.saiprashant.spoartsintractive.DatabaseModule.DataSQLHelper;
import com.saiprashant.spoartsintractive.DatabaseModule.Table_Details;
import com.saiprashant.spoartsintractive.Fragment.MatchDetailsFragment;
import com.saiprashant.spoartsintractive.Fragment.NoteFragment;
import com.saiprashant.spoartsintractive.Fragment.NuggetsFragment;
import com.saiprashant.spoartsintractive.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    public DataSQLHelper dataSQLHelper;
    Context context;
    ProgressDialog pDialog;

    MatchDetailsFragment matchDetailsFragment;
    NuggetsFragment nuggetsFragment;

    NoteFragment noteFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        dataSQLHelper = new DataSQLHelper(context);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...PLease wait");

        matchDetailsFragment = new MatchDetailsFragment(this);
        nuggetsFragment = new NuggetsFragment(this);
        noteFragment = new NoteFragment(this);

        hitApi();

    }

    public BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // By using switch we can easily get
            // the selected fragment
            // by using there id.
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.match_details:
                    selectedFragment = matchDetailsFragment;
                    break;
                case R.id.nugets:
                    selectedFragment = nuggetsFragment;
                    break;
                case R.id.note:
                    selectedFragment = noteFragment;
                    break;

            }
            // It will help to replace the
            // one fragment to other.
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, selectedFragment)
                    .commit();
            return true;
        }
    };





    private void hitApi() {
        String url = "https://cricket.yahoo.net/sifeeds/cricket/live/json/nzin01312019187360.json";

        pDialog.show();
        RequestQueue requestQueue;

        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utility.printMessage(" onResponse response -: " + response.toString());
                try {
                    saveserverDataToDB(new JSONObject(response.toString()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestQueue.stop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.printMessage(" ErrorListener error -: " + error);
                pDialog.hide();
            }
        });

        requestQueue.add(stringRequest);


    }

    public void saveserverDataToDB(JSONObject data) {
        try {
            SQLiteDatabase db = dataSQLHelper.getWritableDatabase();

            if (data.has("Matchdetail") && !data.isNull("Matchdetail")) {
                JSONObject matchdetail_obj = data.getJSONObject("Matchdetail");

                ContentValues values = new ContentValues();
                values.put(Table_Details.Team_Home, matchdetail_obj.getString("Team_Home"));
                values.put(Table_Details.Team_Away, matchdetail_obj.getString("Team_Away"));
                values.put(Table_Details.Match, matchdetail_obj.getString("Match"));
                values.put(Table_Details.Series, matchdetail_obj.getString("Series"));
                values.put(Table_Details.Venue, matchdetail_obj.getString("Venue"));
                values.put(Table_Details.Officials, matchdetail_obj.getString("Officials"));
                values.put(Table_Details.Weather, matchdetail_obj.getString("Weather"));
                values.put(Table_Details.Tosswonby, matchdetail_obj.getString("Tosswonby"));
                values.put(Table_Details.Player_Match, matchdetail_obj.getString("Player_Match"));
                values.put(Table_Details.Result, matchdetail_obj.getString("Result"));

                values.put(Table_Details.Winningteam, matchdetail_obj.getString("Winningteam"));
                values.put(Table_Details.Winmargin, matchdetail_obj.getString("Winmargin"));
                values.put(Table_Details.Equation, matchdetail_obj.getString("Equation"));

                dataSQLHelper.insertRows(db, Table_Details.MatchDetail_Table, values);
            }

            if (data.has("Nuggets") && !data.isNull("Nuggets")) {
                JSONArray array = data.getJSONArray("Nuggets");

                for (int i = 0; i < array.length(); i++) {

                    ContentValues values = new ContentValues();
                    values.put(Table_Details.Nuggets_data, array.get(i).toString());
                    dataSQLHelper.insertRows(db, Table_Details.Nuggets_Table, values);

                }
            }


            if (data.has("Notes") && !data.isNull("Notes")) {
                JSONObject obj = data.getJSONObject("Notes");
                if (obj.has("1") && !obj.isNull("1")) {
                    ContentValues values = new ContentValues();
                    values.put(Table_Details.Note_data, obj.getString("1"));
                    dataSQLHelper.insertRows(db, Table_Details.Note_Table, values);
                }

                if (obj.has("2") && !obj.isNull("2")) {
                    ContentValues values = new ContentValues();
                    values.put(Table_Details.Note_data, obj.getString("2"));
                    dataSQLHelper.insertRows(db, Table_Details.Note_Table, values);
                }

            }

            db.close();
            dataSQLHelper.close();
            pDialog.hide();


            BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
            bottomNav.setOnNavigationItemSelectedListener(navListener);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MatchDetailsFragment(this)).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}