package com.saiprashant.spoartsintractive.DatabaseModule;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.saiprashant.spoartsintractive.Utility.Utility;


public class DataSQLHelper extends SQLiteOpenHelper implements Table_Details {
    private static final String DATABASE_NAME = "MatchDatabase.db";
    private static final int DATABASE_VERSION = 4;
    private Context context;

    public DataSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Utility.printMessage(" Database  Create ");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + MatchDetail_Table + " ("
                + MatchDetail_Table_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + Team_Home + " TEXT, " +
                Team_Away + " TEXT , "
                + Match + " TEXT , " +
                Series + " TEXT , " +
                Venue + " TEXT , " +

                Officials + " TEXT , " +
                Weather + " TEXT , " +
                Tosswonby + " TEXT , " +
                Player_Match + " TEXT , " +
                Result + " TEXT , " +

                Winningteam + " TEXT , " +
                Winmargin + " TEXT , "
                + Equation + " TEXT);");


        db.execSQL("CREATE TABLE IF NOT EXISTS " + Nuggets_Table + " ("
                + Nuggets_Table_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Nuggets_data + " TEXT);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + Note_Table + " ("
                + NoteTable_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Note_data + " TEXT);");



    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;
        onCreate(db);
    }

    public long insertRows(SQLiteDatabase db, String table_name, ContentValues contentValues) {
        long n = db.insert(table_name, null, contentValues);
        Utility.printMessage("Row inserted in " + table_name + " : " + n);
        return n;
    }

    public int delete(String id, String TABLE_NAME, String ID) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereArgs = {id};
        int count = db.delete(TABLE_NAME, ID + " = ?", whereArgs);
        return count;
    }

    public Cursor getvalue_By_colums(SQLiteDatabase db, String table_name,
                                     String select_clause_colunms, String where_clause_column) {
        Cursor cursor = db
                .query(table_name, null, null, null, null, null, null);

        Utility.printMessage("Count :" + table_name + ":" + cursor.getCount());

        return cursor;
    }

    public Cursor getvalue_By_Where(SQLiteDatabase db, String table_name,
                                    String select_clause_colunms, String where_clause_column,
                                    String where_clause_vals) {
        Cursor cursor = db.query(table_name,
                null, where_clause_column,
                new String[]{where_clause_vals}, null, null, null);
        return cursor;
    }

    public long updateRows(SQLiteDatabase db, String table_name,
                           ContentValues values, String whereClause,
                           String whereArgs) {
        long n = db.update(table_name, values, whereClause, new String[]{whereArgs});
        Utility.printMessage("Update :" + table_name + " :" + n);
        return n;
    }


}
