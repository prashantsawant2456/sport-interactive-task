package com.saiprashant.spoartsintractive.DatabaseModule;

public interface Table_Details {

    String MatchDetail_Table = "Matchdetail_Table", MatchDetail_Table_ID = "MatchDetail_Table_ID",
    Team_Home = "Team_Home_data", Team_Away = "Team_Away_data", Match = "Match_data",
            Series = "Series_data", Venue = "Venue_data", Officials = "Officials_data", Weather = "Weather_data", Tosswonby = "Tosswonby_data",
            Player_Match = "Player_Match_data",
            Result = "Result_data",

    Winningteam = "Winningteam_data",
            Winmargin = "Winmargin_data", Equation = "Equation_data";

    String Nuggets_Table = "Nuggets_Table", Nuggets_Table_ID = "Nuggets_Table_ID",
            Nuggets_data = "Nuggets_data";

    String Note_Table = "Note_Table", NoteTable_ID = "NoteTable_ID",
            Note_data = "Note_data";



}
