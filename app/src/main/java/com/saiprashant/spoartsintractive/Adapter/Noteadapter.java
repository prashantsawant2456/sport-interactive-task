package com.saiprashant.spoartsintractive.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saiprashant.spoartsintractive.MainActivity;
import com.saiprashant.spoartsintractive.R;

import java.util.List;

public class Noteadapter extends RecyclerView.Adapter<Noteadapter.ViewHolder> {
    private List<String> list;
    MainActivity activity;

    public Noteadapter(MainActivity mainActivity, List<String> list) {
        this.activity = mainActivity;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nugets_details_list_view, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvDetails.setText(list.get(position));

    }


    @Override
    public int getItemCount() {
         return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDetails;
        public ViewHolder(View itemView) {
            super(itemView);
            tvDetails = itemView.findViewById(R.id.tvDetails);
        }
    }
}
