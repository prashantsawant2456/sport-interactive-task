package com.saiprashant.spoartsintractive.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saiprashant.spoartsintractive.MainActivity;
import com.saiprashant.spoartsintractive.MatchDetailsModel;
import com.saiprashant.spoartsintractive.R;

import org.json.JSONObject;

import java.util.List;

public class MatchDetailsAdapter extends RecyclerView.Adapter<MatchDetailsAdapter.ViewHolder> {
    private List<MatchDetailsModel> list;
    MainActivity activity;

    public MatchDetailsAdapter(MainActivity mainActivity, List<MatchDetailsModel> list) {
        this.activity = mainActivity;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_details_list_view, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MatchDetailsAdapter.ViewHolder holder, int position) {
        final MatchDetailsModel model = list.get(position);
        try {
            JSONObject obj = new JSONObject(model.getOfficials());
            if (obj.has("Umpires") && !obj.isNull("Umpires")) {
                holder.tvUmpires.setText(obj.getString("Umpires"));
            } else {
                holder.tvUmpires.setText("-");
            }

            if (obj.has("Referee") && !obj.isNull("Referee")) {
                holder.tvReferee.setText(obj.getString("Referee"));
            } else {
                holder.tvReferee.setText("-");
            }
            holder.tvWeather.setText(model.getWeather());
            holder.tvPlayer_Match.setText(model.getPlayer_Match());
             holder.tvResult.setText(model.getResult());
             holder.tvWinningteam.setText(model.getStr_Winningteam());
             holder.tvWinmargin.setText(model.getWinmargin());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvUmpires, tvReferee, tvWeather, tvPlayer_Match, tvResult, tvWinningteam, tvWinmargin;

        public ViewHolder(View itemView) {
            super(itemView);

            tvUmpires = itemView.findViewById(R.id.tvUmpires);
            tvReferee = itemView.findViewById(R.id.tvReferee);
            tvWeather = itemView.findViewById(R.id.tvWeather);
            tvPlayer_Match = itemView.findViewById(R.id.tvPlayer_Match);
            tvResult = itemView.findViewById(R.id.tvResult);
            tvWinningteam = itemView.findViewById(R.id.tvWinningteam);
            tvWinmargin = itemView.findViewById(R.id.tvWinmargin);

        }

    }
}
