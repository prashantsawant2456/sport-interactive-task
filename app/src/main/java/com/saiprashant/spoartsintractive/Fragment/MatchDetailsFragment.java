package com.saiprashant.spoartsintractive.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.saiprashant.spoartsintractive.Adapter.MatchDetailsAdapter;
import com.saiprashant.spoartsintractive.DatabaseModule.DataSQLHelper;
import com.saiprashant.spoartsintractive.DatabaseModule.Table_Details;
import com.saiprashant.spoartsintractive.MainActivity;
import com.saiprashant.spoartsintractive.MatchDetailsModel;
import com.saiprashant.spoartsintractive.R;
import com.saiprashant.spoartsintractive.Utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

public class MatchDetailsFragment extends Fragment {
    MainActivity activity;
    RecyclerView rvMathchdetails;
    TextView tvNoDataFound;
    Context context;
    MatchDetailsAdapter matchDetailsAdapter;
    //DataSQLHelper dataSQLHelper;

    public MatchDetailsFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity.getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.match_details_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvMathchdetails = view.findViewById(R.id.rvMathchdetails);
        rvMathchdetails.setLayoutManager(new LinearLayoutManager(context));
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
        //   dataSQLHelper = new DataSQLHelper(context);
        setlistData();
    }

    private void setlistData() {
        ArrayList<MatchDetailsModel> arrayList = new ArrayList<>();
        MatchDetailsModel list_model = new MatchDetailsModel();
        String data = "";
        SQLiteDatabase db = activity.dataSQLHelper.getWritableDatabase();
        Cursor cursor = activity.dataSQLHelper.getvalue_By_colums(db, Table_Details.MatchDetail_Table, null, null);
        while (cursor.moveToNext()) {
            String str_Officials = cursor.getString(cursor.getColumnIndex(Table_Details.Officials));
            String str_Weather = cursor.getString(cursor.getColumnIndex(Table_Details.Weather));
            String str_Player_Match = cursor.getString(cursor.getColumnIndex(Table_Details.Player_Match));
            String str_Result = cursor.getString(cursor.getColumnIndex(Table_Details.Result));
            String str_Winningteam = cursor.getString(cursor.getColumnIndex(Table_Details.Winningteam));
            String str_Winmargin = cursor.getString(cursor.getColumnIndex(Table_Details.Winmargin));
            String str_Equation = cursor.getString(cursor.getColumnIndex(Table_Details.Equation));
            try {

                list_model.setOfficials(str_Officials);
                list_model.setWeather(str_Weather);
                list_model.setPlayer_Match(str_Player_Match);
                list_model.setResult(str_Result);
                list_model.setWinmargin(str_Winmargin);
                list_model.setEquation(str_Equation);
                list_model.setStr_Winningteam(str_Winningteam);
                arrayList.add(list_model);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        matchDetailsAdapter = new MatchDetailsAdapter(activity, arrayList);

        if (arrayList.size() != 0) {
            rvMathchdetails.setAdapter(matchDetailsAdapter);
            rvMathchdetails.setVisibility(View.VISIBLE);
            tvNoDataFound.setVisibility(View.GONE);
        } else {
            rvMathchdetails.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

}
