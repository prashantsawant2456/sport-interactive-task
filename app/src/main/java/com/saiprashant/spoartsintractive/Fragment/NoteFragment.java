package com.saiprashant.spoartsintractive.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.saiprashant.spoartsintractive.Adapter.Noteadapter;
import com.saiprashant.spoartsintractive.Adapter.Nuggetsadapter;
import com.saiprashant.spoartsintractive.DatabaseModule.Table_Details;
import com.saiprashant.spoartsintractive.MainActivity;
import com.saiprashant.spoartsintractive.R;

import java.util.ArrayList;

public class NoteFragment extends Fragment {
    MainActivity activity;
    RecyclerView rvNoteList;
    TextView tvNoDataFound;
    Noteadapter noteadapter;
    Context context;


    public NoteFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity.getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.note_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvNoteList = view.findViewById(R.id.rvNoteList);
        rvNoteList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
        setlistData();
    }

    private void setlistData() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = activity.dataSQLHelper.getWritableDatabase();
        Cursor cursor = activity.dataSQLHelper.getvalue_By_colums(db, Table_Details.Note_Table, null, null);
        if (cursor.moveToFirst()) {
            while ( !cursor.isAfterLast() ) {
                String str_data = cursor.getString(cursor.getColumnIndex(Table_Details.Note_data));
                arrayList.add(str_data);
                cursor.moveToNext();
            }
        }



        noteadapter = new Noteadapter(activity, arrayList);

        if (arrayList.size() != 0) {
            rvNoteList.setAdapter(noteadapter);
            rvNoteList.setVisibility(View.VISIBLE);
            tvNoDataFound.setVisibility(View.GONE);
        } else {
            rvNoteList.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

}
