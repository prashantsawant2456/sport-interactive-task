package com.saiprashant.spoartsintractive.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.saiprashant.spoartsintractive.Adapter.Nuggetsadapter;
import com.saiprashant.spoartsintractive.DatabaseModule.Table_Details;
import com.saiprashant.spoartsintractive.MainActivity;
import com.saiprashant.spoartsintractive.R;

import java.util.ArrayList;

public class NuggetsFragment extends Fragment {
    MainActivity activity;
    RecyclerView rvNuggetsdetails;
    TextView tvNoDataFound;
    Nuggetsadapter nuggetsAdapter;
    Context context;


    public NuggetsFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity.getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nuggets_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvNuggetsdetails = view.findViewById(R.id.rvNuggetsdetails);
        rvNuggetsdetails.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
        setlistData();
    }

    private void setlistData() {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = activity.dataSQLHelper.getWritableDatabase();
        Cursor cursor = activity.dataSQLHelper.getvalue_By_colums(db, Table_Details.Nuggets_Table, null, null);
        if (cursor.moveToFirst()) {
            while ( !cursor.isAfterLast() ) {
                String str_data = cursor.getString(cursor.getColumnIndex(Table_Details.Nuggets_data));
                arrayList.add(str_data);
                cursor.moveToNext();
            }
        }
        nuggetsAdapter = new Nuggetsadapter(activity, arrayList);
        if (arrayList.size() != 0) {
            rvNuggetsdetails.setAdapter(nuggetsAdapter);
            rvNuggetsdetails.setVisibility(View.VISIBLE);
            tvNoDataFound.setVisibility(View.GONE);
        } else {
            rvNuggetsdetails.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

}
